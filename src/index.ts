import * as amqplib from 'amqplib';

class AMQPWrapper {
  private url: string;
  private exchange: string;
  private exchangeType: string;
  private queue: string;
  private defferedQueue: string;
  private deliveryMode: number;
  private xMessageTTL: number;
  private channel: amqplib.Channel;

  private isConfigured: boolean = false;
  private logError: (err: Error) => void;

  constructor() {
    this.exchangeType = 'topic';
    this.deliveryMode = 2;
    this.logError = (err: Error) => null;
  }

  public configure(config: any) {
    this.url = config.url;
    this.exchange = config.exchange;
    this.queue = config.queue;
    this.xMessageTTL = config.xMessageTTL;

    if (config.exchangeType) {
      this.exchangeType = config.exchangeType;
    }

    if (typeof config.deliveryMode !== 'undefined') {
      this.deliveryMode = config.deliveryMode;
    }

    this.isConfigured = true;

    return this;
  }

  public async connect() {
    if (!this.isConfigured) {
      throw new Error('Please configure AMQPWrapper before connect');
    }
    this.channel = await amqplib
      .connect(this.url)
      .then(conn => conn.createChannel());
  }

  public onError(fn: (err: Error) => void) {
    this.logError = (err: Error) => fn(err);
    return this;
  }

  public sendMessageToQueue(data: any, params: any = {}) {
    try {
      const exchange = params.exchange || this.exchange;
      const exchangeType = params.exchangeType || this.exchangeType;
      const queue = params.queue || this.queue;
      const deliveryMode = params.deliveryMode || this.deliveryMode;

      const message = Buffer.from(JSON.stringify(data));

      this.channel.assertQueue(queue);
      this.channel.assertExchange(exchange, exchangeType);
      this.channel.bindQueue(queue, exchange, ''); // http://www.rabbitmq.com/tutorials/tutorial-four-javascript.html
      this.channel.sendToQueue(queue, message, { deliveryMode });
    } catch (err) {
      this.logError(err);
    }
  }

  public sendDeferredMessageToQueue(data: any, params: any = {}) {
    try {
      const exchange = params.exchange || this.exchange;
      const exchangeType = params.exchangeType || this.exchangeType;
      const defferedQueue = params.defferedQueue || this.defferedQueue;
      const deliveryMode = params.deliveryMode || this.deliveryMode;

      const message = Buffer.from(JSON.stringify(data));
      const options = {
        arguments: {
          'x-dead-letter-exchange': exchange,
          'x-dead-letter-routing-key': `${data.type}:${data.action}`,
          'x-expires': this.xMessageTTL * 2,
          'x-message-ttl': this.xMessageTTL
        },
        autoDelete: true,
        durable: true
      };

      this.channel.assertQueue(defferedQueue, options);
      this.channel.assertExchange(exchange, exchangeType);
      this.channel.bindQueue(defferedQueue, exchange, '');
      this.channel.sendToQueue(defferedQueue, message, { deliveryMode });
    } catch (err) {
      this.logError(err);
    }
  }
}

export = new AMQPWrapper();
