interface IAMQPWrapper {
  configure(config: AMQPWrapper.IAMQPWrapperConfig): IAMQPWrapper;
  connect(): Promise;
  onError(fn: (err: Error) => void): IAMQPWrapper;
  sendMessageToQueue(data: any, params?: AMQPWrapper.IChannelParams): void;
  sendDeferredMessageToQueue(data: any, params?: AMQPWrapper.IChannelParams): void;
}

declare namespace AMQPWrapper {

  export enum ExchangeType  {
    direct = 'direct',
    topic = 'topic',
    fanout = 'fanout'
  }

  export enum DeliveryMode  {
    nonpersistent = 1,
    persistent = 2
  }

  export interface IAMQPWrapperConfig {
    url: string;
    exchange: string;
    queue: string;
    defferedQueue: string;
    xMessageTTL: number;
    exchangeType?: ExchangeType;
    deliveryMode?: DeliveryMode
  }

  export interface IChannelParams {
    exchange?: string;
    exchangeType?: ExchangeType;
    queue?: string;
    defferedQueue?: string;
    deliveryMode?: DeliveryMode;
  }
}

declare var amqp: IAMQPWrapper;

export = amqp;
